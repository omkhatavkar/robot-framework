 *** Settings ***
Library     SeleniumLibrary   
Resource    CommonVariables.robot

*** Keywords ***
SETUP
    
      Open Browser    ${URL}    ${BROWSER}
      Maximize Browser Window
      Delete All Cookies
      Set Selenium Implicit Wait    ${IMPLICIT_TIME}
      Set Selenium Timeout    ${TIMEOUT_TIME}
 

LOGIN
    
    Input Text    name=Email    omkhatavkar@gmail.com
    Input Text    name=Password    Omkar_123   
    Click Button    xpath=//input[@class='button-1 login-button' ]  
    
LOGOUT
    Click Link    //a[@href='/logout']
    Close Browser      